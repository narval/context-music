﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {
	public AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TogglePause () {
		if (audioSource.isPlaying)
			audioSource.Pause();
		else
			audioSource.Play();
	}
}
